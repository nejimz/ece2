
<?php
#include('func/session-global-filter.php');
#require_once('func/db-connection.php');
include('func/functions.php');
$user_id = $_SESSION['user']['id'];
$route = 'create';
$name = '';
$wattage = '';

$sqlAppliancies = 'SELECT * FROM appliances ORDER BY name DESC';
$rowAppliances = get($dbConn, $sqlAppliancies, array());

if (isset($_POST['submit'])) {
    $n = 0;
    $errors = [];
    $submit = trim($_POST['submit']);
    $name = trim($_POST['name']);
    $wattage = trim($_POST['wattage']);

    if($name == '') {  
        $errors[$n] = 'Name is required!';
        $n++;
    }
    if ($wattage == '') {
        $errors[$n] = 'Wattage is required!';
        $n++;
    }

    if ($submit == 'create') {
        $sql = 'SELECT COUNT(id) cnt FROM user_appliances WHERE name=:name LIMIT 1';
        $count = count_row($dbConn, $sql, array(':name'=>$name));

        if($count > 0) {
            $errors[$n] = 'Name already exists!';
            $n++;
        }

        if(count($errors) > 0) {
            #$_SESSION['errors'] = $errors;
            #header('Location: appliances-create.php');
            #exit;
            redirect('appliances-create.php?errors=' . json_encode($errors));
        } else {
            $sql = 'INSERT INTO user_appliances (user_id, name, wattage) VALUES (:user_id, :name, :wattage)';
            $values = array(':user_id'=>$user_id, ':name'=>$name, ':wattage'=>$wattage);
            insert($dbConn, $sql, $values);

            #$_SESSION['success'] = 'Appliance successfully added!';
            #header('Location: appliances-create.php');
            #exit;
            redirect('appliances-create.php?success=Appliance successfully added!');
        }
    } elseif ($submit == 'update') {
        $id = trim($_GET['id']);
        $sql = 'SELECT COUNT(id) cnt FROM user_appliances WHERE name=:name AND id<>:id LIMIT 1';
        $count = count_row($dbConn, $sql, array(':name'=>$name, ':id'=>$id));

        if($count > 0) {
            $n++;
            $errors[$n] = 'Name already exists!';
        }

        if(count($errors) > 0) {
            #$_SESSION['errors'] = $errors;
            #header('Location: appliances-create.php?id=' . $id);
            #exit;
            redirect('appliances-create.php?id=' . $id . '&errors=' . json_encode($errors));
        } else {
            $sql = 'UPDATE user_appliances SET user_id=:user_id, name=:name, wattage=:wattage WHERE id=:id';
            $values = array(':user_id'=>$user_id, ':name'=>$name, ':wattage'=>$wattage, ':id'=>$id);
            insert($dbConn, $sql, $values);

            #$_SESSION['success'] = 'Appliance successfully updated!';
            #header('Location: appliances-create.php?id=' . $id);
            #exit;
            redirect('appliances-create.php?id=' . $id . '&success=Appliance successfully updated!');
        }
    }
}

if(isset($_GET['id'])) {
    $id = trim($_GET['id']);
    $sql = 'SELECT * FROM user_appliances WHERE id=:id ORDER BY name DESC LIMIT 1';
    $row = first($dbConn, $sql, array(':id'=>$id));

    $route = 'update';
    $name = $row['name'];
    $wattage = $row['wattage'];
}

include('layouts/header-admin.php');
?>
<h1 class="title is-3"><i class="fa fa-dropbox"></i>&nbsp;Appliances <?php echo ucwords($route); ?></h1>
<br><br>
<?php include('layouts/validation-messages.php'); ?>

<form action="appliances-create.php<?php echo (isset($id))? '?id=' . $id : ''; ?>" method="post">

    <div class="row">
        <div class="col-10">
            <label for="name">Appliance</label>
        </div>
        <div class="col-25">
            <select id="appliance_id" name="appliance_id">
            <option value="">-- Others --</option>
            <?php
            foreach ($rowAppliances as  $row) {
                echo '<option value="' . $row->id . '_-_' . $row->wattage . '">' . $row->name . '</option>';
            }
            ?>
            </select>
        </div>
        <div class="col-5"></div>
        <div class="col-25">
            <input type="text" name="name" id="name" value="<?php echo $name; ?>" >
        </div>
    </div>

    <div class="row">
        <div class="col-10">
            <label for="wattage">Wattage</label>
        </div>
        <div class="col-25">
            <input type="number" name="wattage" id="wattage" value="<?php echo $wattage; ?>">
        </div>
    </div>

    <div class="row">
        <div class="col-10"></div>
        <div class="col-25">
            <button class="btn success" name="submit" value="<?php echo $route; ?>">Submit</button>
        </div>
    </div>
</form>

<script type="text/javascript">
    var xhr_appliance = null;
    $(function(){
        $('#appliance_id').change(function(){
            var appliance = $(this).val().split("_-_");
            var id = appliance[0];
            var wattage = appliance[1];
            var name = $('#appliance_id :selected').text();

            if (id == '') {
                $('#name').val('').attr('readonly', false).focus();
                $('#wattage').val('');
            } else {
                $('#name').val(name).attr('readonly', true);
                $('#wattage').val(wattage);
            }
        });
    });
</script>

<?php include('layouts/footer-admin.php'); ?>