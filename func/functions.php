<?php
require_once('db-connection.php');
include('session-global-filter.php');


function insert($dbConn, $sql, $values)
{
    $stmt = $dbConn->prepare($sql);
    $stmt->execute($values);
}

function count_row($dbConn, $sql, $values)
{
    $stmt = $dbConn->prepare($sql);
    $stmt->execute($values);
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    return $row['cnt'];
}

function first($dbConn, $sql, $values)
{
    $stmt = $dbConn->prepare($sql);
    $stmt->execute($values);
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    return $row;
}

function get($dbConn, $sql, $values)
{
    $stmt = $dbConn->prepare($sql);
    $stmt->execute($values);
    #$row = $stmt->fetch(PDO::FETCH_ASSOC);
    #return $row;
    return $stmt;
}

function role($input)
{
    if($input == 0) {
        return 'Admin';
    }
    return 'Normal';
}

function json($data)
{
    header('Content-type: application/json');
    echo json_encode($data);
}

function redirect($url)
{
    $server_host = 'http://' . $_SERVER['HTTP_HOST'];
    echo '<script>window.location = \'' . $server_host . '/' . $url . '\'</script>';
            exit;
}

function redirect_success($url)
{
    $server_host = 'http://' . $_SERVER['HTTP_HOST'];
    echo '<script>window.location = \'' . $server_host . '/' . $url . '\'</script>';
            exit;
}

function redirect_error($url)
{
    $server_host = 'http://' . $_SERVER['HTTP_HOST'];
    echo '<script>window.location = \'' . $server_host . '/' . $url . '\'</script>';
            exit;
}
?>