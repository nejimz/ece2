-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.2.5-MariaDB-log - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for ece
CREATE DATABASE IF NOT EXISTS `ece` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `ece`;

-- Dumping structure for table ece.advisories
CREATE TABLE IF NOT EXISTS `advisories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `advisories_user_id_foreign` (`user_id`),
  CONSTRAINT `advisories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table ece.advisories: ~0 rows (approximately)
/*!40000 ALTER TABLE `advisories` DISABLE KEYS */;
INSERT INTO `advisories` (`id`, `user_id`, `title`, `description`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Test', 'Test\r\nTest Test\r\nTesting\r\n<h3>Testing</h3>', '2018-08-02 03:30:40', '2018-08-02 03:30:40'),
	(2, 1, 'Test', 'Test\r\nTest Test', '2018-08-02 14:30:40', '2018-08-02 14:30:40'),
	(3, 1, 'Test 2', 'Tes \r\nTes \r\nTE', '2018-08-17 19:41:24', '2018-08-17 19:41:25'),
	(4, 1, 'Mobile Test', 'This is a mobile testing!', '2018-08-17 19:41:27', '2018-08-17 19:41:28');
/*!40000 ALTER TABLE `advisories` ENABLE KEYS */;

-- Dumping structure for table ece.appliances
CREATE TABLE IF NOT EXISTS `appliances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `wattage` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table ece.appliances: ~3 rows (approximately)
/*!40000 ALTER TABLE `appliances` DISABLE KEYS */;
INSERT INTO `appliances` (`id`, `name`, `wattage`) VALUES
	(1, 'Rice Cooker', 150),
	(2, 'Electirc Fan', 100),
	(3, 'Electric Kettle', 180);
/*!40000 ALTER TABLE `appliances` ENABLE KEYS */;

-- Dumping structure for table ece.daily_rate_per_hour
CREATE TABLE IF NOT EXISTS `daily_rate_per_hour` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `rate` double(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table ece.daily_rate_per_hour: ~2 rows (approximately)
/*!40000 ALTER TABLE `daily_rate_per_hour` DISABLE KEYS */;
INSERT INTO `daily_rate_per_hour` (`id`, `date`, `rate`, `created_at`, `updated_at`) VALUES
	(3, '2018-07-01', 10.50, '2018-07-31 03:03:19', '2018-07-31 03:03:19'),
	(29, '2018-08-01', 13.00, '2018-08-07 00:52:42', '2018-08-07 00:52:43');
/*!40000 ALTER TABLE `daily_rate_per_hour` ENABLE KEYS */;

-- Dumping structure for table ece.mobile_auth
CREATE TABLE IF NOT EXISTS `mobile_auth` (
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `role` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table ece.mobile_auth: ~0 rows (approximately)
/*!40000 ALTER TABLE `mobile_auth` DISABLE KEYS */;
INSERT INTO `mobile_auth` (`token`, `user_id`, `role`, `created_at`) VALUES
	('f90d8699f31aae449d8056886f818f3d', 1, 1, '2018-08-17 02:22:16');
/*!40000 ALTER TABLE `mobile_auth` ENABLE KEYS */;

-- Dumping structure for table ece.rates_charges
CREATE TABLE IF NOT EXISTS `rates_charges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `amount` double(10,4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table ece.rates_charges: ~22 rows (approximately)
/*!40000 ALTER TABLE `rates_charges` DISABLE KEYS */;
INSERT INTO `rates_charges` (`id`, `name`, `date`, `amount`, `created_at`) VALUES
	(1, 'Gen Sys Chg', '2018-08-01', 6.9752, '2018-08-20 12:49:07'),
	(2, 'Trans Sys Chg', '2018-08-01', 0.6547, '2018-08-20 12:49:07'),
	(3, 'Sys Loss Chg', '2018-08-01', 1.1298, '2018-08-20 12:49:07'),
	(4, 'Dist Sys Chg', '2018-08-01', 0.2748, '2018-08-20 12:49:07'),
	(5, 'Sup Sys Chg', '2018-08-01', 0.4140, '2018-08-20 12:49:07'),
	(6, 'Mtrg Ret Cust Chg\r\n', '2018-08-01', 5.0000, '2018-08-20 12:49:07'),
	(7, 'Mtrg Sys Chg\r\n', '2018-08-01', 0.3460, '2018-08-20 12:49:07'),
	(8, 'R.F.S.C.\r\n', '2018-08-01', 0.1518, '2018-08-20 12:49:07'),
	(9, 'Lifeline\r\n', '2018-08-01', 0.1038, '2018-08-20 12:49:07'),
	(10, 'Generation Charges VAT', '2018-08-01', 0.7216, '2018-08-20 12:49:07'),
	(11, 'Transmission Chg VAT', '2018-08-01', 0.0691, '2018-08-20 12:49:07'),
	(12, 'System Loss Charges VAT', '2018-08-01', 0.1166, '2018-08-20 12:49:07'),
	(13, 'Distribution Charges VAT', '2018-08-01', 0.1200, '2018-08-20 12:49:07'),
	(14, 'Other Charges VAT', '2018-08-01', 0.1200, '2018-08-20 12:49:07'),
	(15, 'NPC Stranded Debts', '2018-08-01', 0.0265, '2018-08-20 12:49:07'),
	(16, 'NPC SC Costs', '2018-08-01', 0.1938, '2018-08-20 12:49:07'),
	(17, 'M.E.', '2018-08-01', 0.1544, '2018-08-20 12:49:07'),
	(18, 'UCME For R.E.D.', '2018-08-01', 0.0017, '2018-08-20 12:49:07'),
	(19, 'F.I.T. All', '2018-08-01', 0.2563, '2018-08-20 12:49:07'),
	(20, 'Environment Chg', '2018-08-01', 0.0025, '2018-08-20 12:49:07'),
	(21, 'S.C. Subsidy', '2018-08-01', 0.0007, '2018-08-20 12:49:07'),
	(22, 'ERC Case 2013-141 Rc 0.0817', '2018-08-01', 0.0817, '2018-08-20 12:49:07');
/*!40000 ALTER TABLE `rates_charges` ENABLE KEYS */;

-- Dumping structure for table ece.usage
CREATE TABLE IF NOT EXISTS `usage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT 0,
  `appliance_id` int(11) DEFAULT 0,
  `start_datetime` datetime DEFAULT current_timestamp(),
  `end_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table ece.usage: ~4 rows (approximately)
/*!40000 ALTER TABLE `usage` DISABLE KEYS */;
INSERT INTO `usage` (`id`, `user_id`, `appliance_id`, `start_datetime`, `end_datetime`) VALUES
	(1, 1, 3, '2018-08-21 20:34:26', NULL),
	(2, 1, 2, '2018-08-24 18:27:57', NULL),
	(3, 1, 1, '2018-08-24 18:39:24', '2018-08-24 19:05:12'),
	(4, 1, 1, '2018-08-24 19:06:15', NULL),
	(5, 1, 9, '2018-08-24 20:38:58', '2018-08-24 20:38:59'),
	(6, 1, 9, '2018-08-24 20:39:07', '2018-08-24 20:39:17'),
	(7, 1, 7, '2018-08-24 20:39:16', NULL);
/*!40000 ALTER TABLE `usage` ENABLE KEYS */;

-- Dumping structure for table ece.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `role` tinyint(4) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table ece.users: ~9 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Mark Anthony Mondres', 'mark.mondres@panasiaticcallcenters.com', '5f4dcc3b5aa765d61d8327deb882cf99', 0, 'ML5ett9wxex2TvF5Efy2UYoNrqmkDWy4RBwRqNFDNkJK14MC9nm2lrRMjHEj', '2018-07-10 10:11:49', '2018-08-20 18:32:10'),
	(3, 'Eldred Kenneth Torrento', 'eldred.torrento@panasiaticcallcenters.com', '161ebd7d45089b3446ee4e0d86dbcf92', 0, NULL, '2018-07-10 12:49:14', '2018-07-10 14:38:41'),
	(4, 'Amber Dana Dawn Goylos', 'amber.goylos@panasiaticcallcenters.com', '161ebd7d45089b3446ee4e0d86dbcf92', 1, NULL, '2018-07-18 02:25:27', '2018-07-18 02:25:27'),
	(5, 'Jay-Arr Gonzales', 'jay.gonzales@panasiaticcallcenters.com', '161ebd7d45089b3446ee4e0d86dbcf92', 1, NULL, '2018-08-03 19:38:35', '2018-08-03 19:38:36'),
	(6, 'Jezreel Cajeda', 'jezreel.cajeda@panasiaticcallcenters.com', '369ca48290460b75b4d7b182e43a97dd', 0, NULL, '2018-08-06 17:22:25', '2018-08-06 17:31:59'),
	(7, 'Ansley Troy Quinit', 'ansley.quinit@panasiaticcallcenters.com', '5f4dcc3b5aa765d61d8327deb882cf99', 1, NULL, '2018-08-17 02:03:27', '2018-08-17 02:03:27'),
	(8, 'Reynante Francisco', 'reynante.francisco@panasiaticcallcenters.com', '5f4dcc3b5aa765d61d8327deb882cf99', 1, NULL, '2018-08-16 18:05:45', '2018-08-16 18:05:45'),
	(9, 'Adam Patrick Alo', 'adam.alo@panasiaticcallcenters.com', '5f4dcc3b5aa765d61d8327deb882cf99', 1, NULL, '2018-08-16 18:30:41', '2018-08-16 18:30:41'),
	(10, 'Cardo D. Dalisay', 'cd@panasiaticcallcenters.com', '5f4dcc3b5aa765d61d8327deb882cf99', 1, NULL, '2018-08-17 13:26:27', '2018-08-17 14:10:29'),
	(11, 'Tester Test', 'test@test.com', '81dc9bdb52d04dc20036dbd8313ed055', 1, NULL, '2018-08-23 19:23:21', '2018-08-23 19:23:21');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table ece.user_appliances
CREATE TABLE IF NOT EXISTS `user_appliances` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wattage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_appliances_user_id_foreign` (`user_id`),
  CONSTRAINT `user_appliances_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table ece.user_appliances: ~8 rows (approximately)
/*!40000 ALTER TABLE `user_appliances` DISABLE KEYS */;
INSERT INTO `user_appliances` (`id`, `user_id`, `name`, `wattage`, `image`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Electric Fan', '55', '', '2018-07-10 19:17:55', '2018-07-31 02:45:18'),
	(2, 1, 'Air Con', '85', '', '2018-07-10 19:29:20', '2018-07-31 02:45:31'),
	(3, 1, 'a3', '38', '', '2018-07-18 02:37:46', '2018-07-18 02:37:46'),
	(4, 1, 'Television', '45', '', '2018-07-26 04:45:30', '2018-07-31 02:45:41'),
	(5, 1, 'Test Cooker', '125', 'images/appliances/520180808080951.jpg', NULL, NULL),
	(6, 1, 'Rice Test', '300', NULL, NULL, NULL),
	(7, 1, 'Heater', '100', NULL, NULL, NULL),
	(8, 1, 'Rice Cooker', '200', NULL, NULL, NULL),
	(9, 1, 'Electric Kettle', '180', NULL, NULL, NULL);
/*!40000 ALTER TABLE `user_appliances` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
