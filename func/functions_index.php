<?php
session_start();
require_once('db-connection.php');

/*if(isset($_SESSION['user']))
{
	header('Location: home.php');
    exit;
}*/

function insert($dbConn, $sql, $values)
{
    $stmt = $dbConn->prepare($sql);
    $stmt->execute($values);
}

function count_row($dbConn, $sql, $values)
{
    $stmt = $dbConn->prepare($sql);
    $stmt->execute($values);
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    return $row['cnt'];
}

function first($dbConn, $sql, $values)
{
    $stmt = $dbConn->prepare($sql);
    $stmt->execute($values);
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    return $row;
}

function get($dbConn, $sql, $values)
{
    $stmt = $dbConn->prepare($sql);
    $stmt->execute($values);
    #$row = $stmt->fetch(PDO::FETCH_ASSOC);
    #return $row;
    return $stmt;
}

function role($input)
{
    if($input == 0) {
        return 'Admin';
    }
    return 'Normal';
}

function json($data)
{
    header('Content-type: application/json');
    echo json_encode($data);
    exit;
}

function mobile_auth_check($dbConn, $values)
{
    $sql = 'SELECT COUNT(token) AS cnt FROM mobile_auth WHERE token=:token LIMIT 1';
    $count = count_row($dbConn, $sql, $values);

    if ($count > 0) {
        $data = array('avail' => 1);
    } else {
        $data = array('avail' => 0);
    }

    return $data;
}

function toArray($rows)
{
    $n = 0;
    $array = array();
    foreach($rows as $row) {
        $array[$n] = $row;
        $n++;
    }

    return $array;
}

function mobile_access($dbConn, $token)
{
    $sql = 'SELECT * FROM mobile_auth WHERE token=:token LIMIT 1';
    $row = first($dbConn, $sql, array(':token'=>$token));

    return $row;
}
?>