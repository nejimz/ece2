<?php
$host = 'localhost';
$database = 'ece';
$charset = 'utf8';
$username = 'root';
$password = '';

$dbConn = new PDO("mysql:dbname=$database;host=$host;charset=$charset", $username, $password);
$dbConn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$dbConn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
#$dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
$dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

?>