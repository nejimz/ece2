<?php
include('../func/functions_index.php');

if(!isset($_GET['token'])) {
    json('no token');
}
if(isset($_GET['token'])) {
	$token = trim($_GET['token']);
    $countToken = mobile_auth_check($dbConn, array( ':token' => $token ));

    if ($countToken['avail'] == 0) {
    	json('invalid token');
    }
}

$ouser = mobile_access($dbConn, $token);

$sqlUser = 'SELECT * FROM users WHERE id=:id LIMIT 1';
$userRow = first($dbConn, $sqlUser, array('id' => $ouser['user_id']));
#json($userRow);
$user_id = $userRow['id'];
$name = $userRow['name'];
$email = $userRow['email'];
$role = $userRow['role'];
