
<?php
#include('func/session-global-filter.php');
#require_once('func/db-connection.php');
include('func/functions.php');

if(isset($_GET['action']))
{
    $sqlDelete = 'UPDATE user_appliances SET deleted_at=:deleted_at WHERE id=:id';
    $values = array(':deleted_at'=>date('Y-m-d H:i:s'), ':id'=>$_GET['id']);
    insert($dbConn, $sqlDelete, $values);
    redirect('appliances.php?warning=Appliance successfully remove!');
}

$search = '';

$sql = 'SELECT * FROM user_appliances WHERE deleted_at IS NULL AND user_id=:user_id ';
$values = array(':user_id'=>$_SESSION['user']['id']);
if(isset($_GET['search']))
{
    $search = trim($_GET['search']);
    $sql .= 'WHERE name LIKE :name ';
    $values[':name'] = "%$search%";
}

$sql .= 'ORDER BY name DESC';

$rows = get($dbConn, $sql, $values);

include('layouts/header-admin.php');
?>
<h1 class="title is-3"><i class="fa fa-dropbox"></i>&nbsp;Appliances</h1>
<form action="" method="get">
    <div class="row">
        <div class="col-15">
            <input class="input is-normal" type="text" name="search" id="search" value="<?php echo $search; ?>">
        </div>
        <div class="col-15">
            <button class="btn success"><i class="fa fa-search"></i>Search</button>
        </div>
        <div class="col-70"><?php include('layouts/validation-messages.php'); ?></div>
    </div>
</form>

<table>
    <thead>
        <tr>
            <th width="10%" class="text-center">
                <a href="appliances-create.php"><i class="fa fa-plus-circle fa-lg"></i></a>
            </th>
            <th width="70%">Name</th>
            <th width="20%" class="text-center">Wattage</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($rows as $row) { ?>
        <tr>
            <td class="text-center">
                <a href="appliances-create.php?id=<?php echo $row->id; ?>" title="Edit Appliance Details">
                    <i class="fa fa-edit fa-lg"></i>
                </a>
                <a href="appliances-upload-image.php?id=<?php echo $row->id; ?>" title="Upload Appliance Image">
                    <i class="fa fa-image fa-lg"></i>
                </a>
                <a href="appliances.php?action=delete&id=<?php echo $row->id; ?>" title="Delete Appliance">
                    <i class="fa fa-times fa-lg"></i>
                </a>
            </td>
            <td><?php echo $row->name; ?></td>
            <td class="text-center"><?php echo $row->wattage; ?></td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<?php include('layouts/footer-admin.php'); ?>