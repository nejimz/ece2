<?php 
#require_once('func/db-connection.php');
include('func/functions_index.php');
#echo md5('password');
if(isset($_POST['email']))
{
    $email = trim($_POST['email']);
    $password = trim($_POST['password']);
    $password = md5($password);

    $sql = 'SELECT * FROM users WHERE email=:email AND password=:password LIMIT 1';
    $user = first($dbConn, $sql, array( ':email' => $email, ':password' => $password ));
    
    if($user) {
        $_SESSION['user'] = $user;
        header('Location: home.php');
        exit;
    } else {
        header('Location: index.php?error=Invalid credentials!');
        exit;
    }
}

include('layouts/header.php');
?>
    <div class="grid-container">
        <div class="item1"></div>
        <div class="item1">
            <div class="login-border">
                <h1 class="login-title">ECE</h1>
                <h4 class="login-title-sub">Electricity<br>Consumption<br>Estimator</h4>
                <form class="login-form" method="POST" action="index.php">
                    <?php echo (isset($_GET['error'])) ? '<p class="login-warning-message">' . $_GET['error'] . '</p>' : ''; ?>
                    <input id="email" type="email" name="email" placeholder="E-Mail" value="" required autofocus><br><br>
                    <input id="password" type="password" name="password" placeholder="Password" required>
                    <br><br>
                    <button type="submit" class="btn success">Login</button>
                </form>
                <p>Don't have account? <a href="register.php">Register Now!</a></p>
            </div>
        </div>
        <div class="item1"></div>
    </div>
<?php include('layouts/footer.php'); ?>
