<?php
#include('func/session-global-filter.php');
#require_once('func/db-connection.php');
include('func/functions.php');
$user_id = $_SESSION['user']['id'];
$route = 'create';
$name = '';
$wattage = '';

if (isset($_POST['submit'])) {
    $n = 0;
    $errors = [];
    $submit = trim($_POST['submit']);
    $name = trim($_POST['name']);
    $wattage = trim($_POST['wattage']);

    if($name == '')
    {  
        $errors[$n] = 'Name is required!';
        $n++;
    }
    if ($wattage == '') {
        $errors[$n] = 'Wattage is required!';
        $n++;
    }

    if(count($errors) > 0) {
        #$_SESSION['errors'] = $errors;
        #header('Location: appliance-create.php');
        #exit;
        redirect('appliance-create.php?errors=' . json_encode($errors));
    }

    if ($submit == 'create') {
        $sql = 'INSERT INTO appliances (name, wattage) VALUES (:name, :wattage)';
        $values = array(':name'=>$name, ':wattage'=>$wattage);
        insert($dbConn, $sql, $values);

        #$_SESSION['success'] = 'Appliance successfully added!';
        #header('Location: appliance-create.php');
        #exit;
        redirect('appliance-create.php?success=Appliance successfully added!');
    } elseif ($submit == 'update') {
        $id = trim($_GET['id']);
        $sql = 'UPDATE appliances SET name=:name, wattage=:wattage WHERE id=:id';
        $values = array(':name'=>$name, ':wattage'=>$wattage, ':id'=>$id);
        insert($dbConn, $sql, $values);

        #$_SESSION['success'] = 'Appliance successfully updated!';
        #header('Location: appliance-create.php?id=' . $id);
        #exit;
        redirect('appliance-create.php?id=' . $id . '&success=Appliance successfully updated!');
    }
}

if(isset($_GET['id'])) {
    $id = trim($_GET['id']);
    $sql = 'SELECT * FROM appliances WHERE id=:id LIMIT 1';
    $row = first($dbConn, $sql, array(':id'=>$id));

    $route = 'update';
    $name = $row['name'];
    $wattage = $row['wattage'];
}

include('layouts/header-admin.php');
?>
<h1 class="title is-3"><i class="fa fa-file"></i>&nbsp;Appliance <?php echo ucwords($route); ?></h1>
<br><br>
<?php include('layouts/validation-messages.php'); ?>

<form action="appliance-create.php<?php echo (isset($id))? '?id=' . $id : ''; ?>" method="post">

    <div class="row">
        <div class="col-25">
            <label for="name">Name</label>
            <input class="input is-normal" type="text" name="name" id="name" value="<?php echo $name; ?>">
        </div>
    </div>

    <div class="row">
        <div class="col-25">
            <label for="wattage">Wattage</label>
            <input class="input is-normal" type="text" name="wattage" id="wattage" value="<?php echo $wattage; ?>">
        </div>
    </div>

    <div class="row">
        <div class="col-25">
            <br>
            <button class="btn success" name="submit" value="<?php echo $route; ?>">Submit</button>
        </div>
    </div>
</form>

<?php include('layouts/footer-admin.php'); ?>