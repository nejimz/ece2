<?php
include('func/functions.php');

if (isset($_POST['appliance_id'])) {
	$user_id = $_SESSION['user']['id'];
	$appliance_id = trim($_POST['appliance_id']);
	$dateTimeNow = date('Y-m-d H:i:s');

	$sql = 'UPDATE `usage` SET end_datetime=:end_datetime WHERE user_id=:user_id AND appliance_id=:appliance_id AND end_datetime IS NULL';
	$values = array(':user_id'=>$user_id, ':appliance_id'=>$appliance_id, ':end_datetime'=>$dateTimeNow);

	insert($dbConn, $sql, $values);

	$data = array('appliance_id'=>$appliance_id, 'running_time' => '0 days 00 hrs 0 mins');

	json($data);
}