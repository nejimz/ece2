
<?php
#include('func/session-global-filter.php');
#require_once('func/db-connection.php');
include('func/functions.php');
$user_id = $_SESSION['user']['id'];
$route = 'create';
$title = '';
$description = '';

if (isset($_POST['submit'])) {
    $n = 0;
    $errors = [];
    $submit = trim($_POST['submit']);
    $title = trim($_POST['title']);
    $description = trim($_POST['description']);

    if($title == '')
    {  
        $n++;
        $errors[$n] = 'Title is required!';
    }
    if ($description == '') {
        $n++;
        $errors[$n] = 'Description is required!';
    }

    if(count($errors) > 0) {
        #$_SESSION['errors'] = $errors;
        #header('Location: advisories-create.php');
        #exit;
        redirect('advisories-create.php?errors=' . json_encode($errors));
    }

    if ($submit == 'create') {
        $sql = 'INSERT INTO advisories (user_id, title, description) VALUES (:user_id, :title, :description)';
        $values = array(':user_id'=>$user_id, ':title'=>$title, ':description'=>$description);
        insert($dbConn, $sql, $values);

        #$_SESSION['success'] = 'Advisories successfully added!';
        #header('Location: advisories-create.php');
        #exit;
        redirect('advisories-create.php?success=Advisories successfully added!');
    } elseif ($submit == 'update') {
        $id = trim($_GET['id']);
        $sql = 'UPDATE advisories SET user_id=:user_id, title=:title, description=:description WHERE id=:id';
        $values = array(':user_id'=>$user_id, ':title'=>$title, ':description'=>$description, ':id'=>$id);
        insert($dbConn, $sql, $values);

        #$_SESSION['success'] = 'Advisories successfully updated!';
        #header('Location: advisories-create.php?id=' . $id);
        #exit;
        redirect('advisories-create.php?id=' . $id . '&success=Advisories successfully updated!');
    }
}

if(isset($_GET['id'])) {
    $id = trim($_GET['id']);
    $sql = 'SELECT * FROM advisories WHERE id=:id ORDER BY created_at DESC LIMIT 1';
    $row = first($dbConn, $sql, array(':id'=>$id));

    $route = 'update';
    $title = $row['title'];
    $description = $row['description'];
}

include('layouts/header-admin.php');
?>
<h1 class="title is-3"><i class="fa fa-file"></i>&nbsp;Advisories <?php echo ucwords($route); ?></h1>
<br><br>
<?php include('layouts/validation-messages.php'); ?>

<form action="advisories-create.php<?php echo (isset($id))? '?id=' . $id : ''; ?>" method="post">

    <div class="row">
        <div class="col-25">
            <label for="title">Title</label>
            <input class="input is-normal" type="text" name="title" id="title" value="<?php echo $title; ?>">
        </div>
    </div>

    <div class="row">
        <div class="col-100">
            <label for="description">Description</label>
            <textarea rows="10" class="textarea" id="description" name="description"><?php echo $description; ?></textarea>
        </div>
    </div>

    <div class="row">
        <div class="col-10"></div>
        <div class="col-25">
            <button class="btn success" name="submit" value="<?php echo $route; ?>">Submit</button>
        </div>
    </div>

</form>
<?php include('layouts/footer-admin.php'); ?>