
<?php
include('func/functions.php');

$search = '';

$sql = 'SELECT * FROM rates_charges ORDER BY date DESC LIMIT 50';
$values = array();
$rows = get($dbConn, $sql, $values);

include('layouts/header-admin.php');
?>
<h1 class="title is-3"><i class="fa fa-dollar"></i>&nbsp;Rate Charges</h1>
<!-- <form action="" method="get">
    <div class="row">
        <div class="col-15">
            <input class="input is-normal" type="text" name="search" id="search" value="<?php #echo $search; ?>">
        </div>
        <div class="col-15">
            <button class="btn success"><i class="fa fa-search fa-lg"></i>Search</button>
        </div>
    </div>
</form> -->
<table>
    <thead>
        <tr>
            <th width="5%" class="text-center"><!-- <a href="rate-charges-create.php"><i class="fa fa-plus-circle fa-lg"></i></a> --></th>
            <th width="35%">Date</th>
            <th width="20%" class="text-center">Date</th>
            <th width="20%" class="text-right">Rate</th>
            <th width="20%">Created At</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($rows as $row) { ?>
        <tr>
            <td class="text-center">
            <?php
            if($_SESSION['user']['role'] == 0) {
            ?>
            <a href="rate-charges-create.php?id=<?php echo $row->id; ?>" title="Edit Rate Details"><i class="fa fa-edit fa-lg"></i></a>
            <?php
            } else {
            ?>
            <i class="fa fa-edit"></i>
            <?php
            }
            ?>
            </td>
            <td><?php echo $row->name; ?></td>
            <td class="text-center"><?php echo date('Y M', strtotime($row->date)); ?></td>
            <td class="text-right"><?php echo number_format($row->amount, 2); ?></td>
            <td><?php echo $row->created_at; ?></td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<?php include('layouts/footer-admin.php'); ?>