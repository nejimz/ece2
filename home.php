
<?php
#include('func/session-global-filter.php');
include('func/functions.php');

$sql = 'SELECT * FROM advisories ORDER BY created_at DESC LIMIT 10';
$values = array();

$rows = get($dbConn, $sql, $values);

include('layouts/header-admin.php');
?>
<h1><i class="fa fa-home"></i>&nbsp;Home</h1>
<?php foreach($rows as  $row){ ?>
<div class="row">
    <div class="col-100 pnl-advisory">
        <h3><?php echo nl2br($row->title); ?></h3><br>
        <h5><?php echo nl2br($row->description); ?></h5><br>
        <h6><small><br /><?php echo $row->created_at; ?></small></h6>
    </div>
</div> 
<?php } ?>
<?php include('layouts/footer-admin.php'); ?>