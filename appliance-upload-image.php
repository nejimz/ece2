
<?php
#include('func/session-global-filter.php');
#require_once('func/db-connection.php');
include('func/functions.php');
$user_id = $_SESSION['user']['id'];

if(!isset($_GET['id'])) {
    exit;
}

$id = trim($_GET['id']);
$sql = 'SELECT * FROM appliances WHERE id=:id ORDER BY name DESC LIMIT 1';
$row = first($dbConn, $sql, array(':id'=>$id));

if(!$row) {
    exit;
}

if (isset($_POST['submit'])) {
    $n = 0;
    $errors = [];
    $file_name = $_FILES['image']['name'];
    #$file_ext = explode('.', string)
    $file_ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));
    $file_type = $_FILES['image']['type'];
    $file_size = $_FILES['image']['size'];
    $file_temp = $_FILES['image']['tmp_name'];
    $location = 'images/appliances/';
    $target_file = $location . '' . $id . '' . date('Ymdhis') . '.' . $file_ext;

    if($file_name == '') {  
        $n++;
        $errors[$n] = 'Image is required!';
    }

    if(count($errors) > 0) {
        #$_SESSION['errors'] = $errors;
        #header('Location: appliance-upload-image.php?id=' . $id);
        redirect('appliance-upload-image.php?id=' . $id . '&errors=' . json_encode($errors));
        exit;
    }

    move_uploaded_file($file_temp, $target_file);

    $sql = 'UPDATE appliances SET image=:image WHERE id=:id';
    $values = array(':image'=>$target_file, ':id'=>$id);
    insert($dbConn, $sql, $values);

    #$_SESSION['success'] = 'Appliance successfully updated!';
    #header('Location: appliance-upload-image.php?id=' . $id);
    redirect('appliance-upload-image.php?id=' . $id . '&success= successfully updated!');
    exit;
}

$image = 'images/appliances/no-image.jpg';

if($row['image'] != '') {
    $image = $row['image'];
}

include('layouts/header-admin.php');
?>
<h1 class="title is-3"><i class="fa fa-dropbox"></i>&nbsp;Appliances - <?php echo $row['name']; ?></h1>
<br><br>
<?php include('layouts/validation-messages.php'); ?>

<div class="row">
    <div class="col-30">
        <form action="appliance-upload-image.php?id=<?php echo $id; ?>" method="post" enctype="multipart/form-data">
            <input class="file-input" type="file" name="image" accept="image/*" />
            <br><br>
            <button class="btn success" name="submit">Upload</button>
        </form>
    </div>
    <div class="col-5"></div>
    <div class="col-65">
        <img src="<?php echo $image; ?>" width="200px">
    </div>
</div>
<?php include('layouts/footer-admin.php'); ?>