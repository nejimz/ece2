
<?php
#include('func/session-global-filter.php');
#require_once('func/db-connection.php');
include('func/functions.php');


$sql = 'SELECT * FROM rates_charges WHERE id=:id ORDER BY date DESC LIMIT 1';
$rate = first($dbConn, $sql, array(':id'=>1));

$sql = 'SELECT * FROM user_appliances WHERE user_id=:user_id ORDER BY name ASC';
$appliances = get($dbConn, $sql, array(':user_id'=>$_SESSION['user']['id']));

$appliances_option = '';
foreach($appliances as $appliance){
    $appliances_option .= '<option value="' . $appliance->wattage . '_-_' . $appliance->name . '">' . $appliance->name . '</option>';
}


$rows1 = get($dbConn, 'SELECT * FROM rates_charges WHERE id IN (1, 2, 3) ORDER BY id ASC', array());
$rows2 = get($dbConn, 'SELECT * FROM rates_charges WHERE id IN (4, 5, 6, 7) ORDER BY id ASC', array());
$rows3 = get($dbConn, 'SELECT * FROM rates_charges WHERE id IN (8, 9) ORDER BY id ASC', array());
$rows4 = get($dbConn, 'SELECT * FROM rates_charges WHERE id IN (10,11,12) ORDER BY id ASC', array());
$rows5 = get($dbConn, 'SELECT * FROM rates_charges WHERE id IN (15,16,17,18,19,20,21,22) ORDER BY id ASC', array());


include('layouts/header-admin.php');
?>
<h1 class="title is-3"><i class="fa fa-calculator"></i>&nbsp;Calculator</h1>
<div class="row">
    <div class="col-30">
        <form id="calculator" action="#" method="post">
            <div class="row">
                <div class="col-25">
                    <label for="fname">Amount</label>
                </div>
                <div class="col-75">
                    <input class="input is-large" id="price" name="price" type="text" placeholder="Price" autocomplete="off" value="<?php echo ($rate)? $rate['amount'] : 0; ?>" readonly="">
                </div>
            </div>

            <div class="row">
                <div class="col-25">
                    <label for="fname">Appliance</label>
                </div>
                <div class="col-75">
                    <select id="appliance_id" name="appliance_id" autofocus="">
                        <option value="">- Appliance -</option>
                        <?php echo $appliances_option; ?>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-25">
                    <label for="wattage">Wattage</label>
                </div>
                <div class="col-75">
                    <input class="input is-large" id="wattage" name="wattage" type="text" placeholder="Wattage" autocomplete="off">
                </div>
            </div>

            <div class="row">
                <div class="col-25">
                    <label for="hours">Hours</label>
                </div>
                <div class="col-75">
                    <input class="input is-large" id="hours" name="hours" type="text" placeholder="Hour/s" autocomplete="off">
                </div>
            </div>

            <div class="row">
                <div class="col-25">
                    <label for="days">Days</label>
                </div>
                <div class="col-75">
                    <input class="input is-large" id="days" name="days" type="text" placeholder="Hour/s" autocomplete="off">
                </div>
            </div>

            <div class="row">
                <div class="col-25">&nbsp;</div>
                <div class="col-75">
                    <button class="btn success" type="submit"><i class="fa fa-calculator fa-lg"></i>&nbsp;Calculate</button>
                    <button class="btn danger" type="reset"><i class="fa fa-refresh fa-lg"></i>&nbsp;Reset</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-70">
        <table id="appliances" class="table-margin-left">
            <thead>
                <tr>
                    <th width="5%"></th>
                    <th width="55%">Appliance</th>
                    <th width="10%" class="text-center">Wattage</th>
                    <th width="10%" class="text-center">Hour/s</th>
                    <th width="10%" class="text-center">Day/s</th>
                    <th width="10%" class="text-right">Cost</th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
                <tr>
                    <th colspan="3"></th>
                    <th colspan="2">Total Energy</th>
                    <th id="total-energy" class="text-right"></th>
                </tr>
                <!-- <tr>
                    <th colspan="3"></th>
                    <th colspan="2">Monthly Bill</th>
                    <th id="monthly-bill" class="text-right"></th>
                </tr> -->
            </tfoot>
        </table>
        <table class="table-margin-left">
            <tr>
                <th width="40%">Charges</th>
                <th width="30%">Rate</th>
                <th width="30%" class="text-right">Amount</th>
            </tr>
            <?php
            foreach ($rows1 as $row) {
                echo '<tr><td>' . $row->name . '</td><td id="chargeRate' . $row->id . '">' . $row->amount . '</td><td id="chargeAmount' . $row->id . '" class="text-right"></td></tr>';
            }
            ?>
            <tr>
                <th class="text-right" colspan="2">Subtotal</th>
                <th class="text-right" id="chargeTotalAmount1"></th>
            </tr>
            <?php
            foreach ($rows2 as $row) {
                echo '<tr><td>' . $row->name . '</td><td id="chargeRate' . $row->id . '">' . $row->amount . '</td><td id="chargeAmount' . $row->id . '" class="text-right"></td></tr>';
            }
            ?>
            <tr>
                <th class="text-right" colspan="2">Subtotal</th>
                <th class="text-right" id="chargeTotalAmount2"></th>
            </tr>
            <?php
            foreach ($rows3 as $row) {
                echo '<tr><td>' . $row->name . '</td><td id="chargeRate' . $row->id . '">' . $row->amount . '</td><td id="chargeAmount' . $row->id . '" class="text-right"></td></tr>';
            }
            ?>
            <tr>
                <th class="text-right" colspan="2">Subtotal</th>
                <th class="text-right" id="chargeTotalAmount3"></th>
            </tr>
            <?php
            foreach ($rows5 as $row) {
                echo '<tr><td>' . $row->name . '</td><td id="chargeRate' . $row->id . '">' . $row->amount . '</td><td id="chargeAmount' . $row->id . '" class="text-right"></td></tr>';
            }
            ?>
            <tr>
                <th class="text-right" colspan="2">Subtotals</th>
                <th class="text-right" id="chargeTotalAmount4"></th>
            </tr>
        </table>
    </div>
</div>


<script type="text/javascript">
$(function(){
    $('#appliance_id').change(function(){
        var appliance = $(this).val().split("_-_");
        $('#wattage').val(appliance[0]);
    });

    $('#calculator').submit(function(){
        var id = Math.floor(Math.random() * 10000);
        var icon = '<a href="javascript:void(0)" onclick="deleteRow(this)"><i class="fa fa-times fa-lg"></i></a>';
        var appliance = $('#appliance_id').val().split("_-_");
        var wattage = $('#wattage').val().trim();
        var hours = $('#hours').val().trim();
        var days = $('#days').val();
        var price = $('#price').val();

        if(price == '' || price == 0)
        {
            alert('Price is required!');
            return false;
        }
        else if(!isFloat(price) && isNaN(price))
        {
                alert('Price is must be a number!');
                return false;
        }
        if(appliance == '')
        {
            alert('Appliance is required!');
            return false;
        }
        if(isNaN(wattage) || wattage == '' || wattage == 0)
        {
            alert('Wattage is required!');
            return false;
        }
        if(isNaN(hours) || hours == '')
        {
            alert('Hour/s is required!');
            return false;
        }

        else if(hours < 1)
        {
            alert('Hour/s is minimum of 1!');
            return false;
        }
        else if(hours > 24)
        {
            alert('Hour/s is maximum of 24!');
            return false;
        }

        if(isNaN(days) || days == '' || days == 0)
        {
            alert('Day/s is required!');
            return false;
        }
        else if(days < 1)
        {
            alert('Day/s is minimum of 1!');
            return false;
        }
        else if(days > 30)
        {
            alert('Hour/s is maximum of 30!');
            return false;
        }

        var kwh = (wattage * hours * days) / 1000;
        var cost = (kwh * price).toFixed(2);
        var row =   '<tr id="row' + appliance[0] + '' + id + '"><td class="text-center">' + icon + '</td>' + 
                    '<td>' + appliance[1] + '</td>' + 
                    '<td class="text-center wattage-number">' + wattage + '</td>' + 
                    '<td class="text-center">' + hours + '</td>' + 
                    '<td class="text-center">' + days + '</td>' + 
                    '<td class="text-right cost">' + cost + '</td></tr>';

        $('#appliances tbody').append(row);
        monthly_bill();
        return false;
    });
});
function isFloat(n)
{
    return Number(n) === n && n % 1 !== 0;
}
function deleteRow(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("appliances").deleteRow(i);
    monthly_bill();
} 
function monthly_bill() {
    var hours = $('#hours').val().trim();
    var days = $('#days').val();
    var price = $('#price').val();

    var total_kwh = 0;
    var monthly_bill = 0;
    var monthly_bill = 0;

    $('#appliances tbody tr').each(function(key, value){
        var last_child = value['lastElementChild']['textContent'];
        var cost = parseFloat(last_child);
        monthly_bill = cost + monthly_bill;
    });

    $('#appliances tbody tr td.wattage-number').each(function(key, value){
        var wattage = parseFloat(value['textContent']);
        var kwh = (wattage * hours * days) / 1000;
        total_kwh += kwh;
    });

    var gen_sys_rate = parseFloat($('#chargeRate1').text());
    var trans_sys_rate = parseFloat($('#chargeRate2').text());
    var sys_loss_rate = parseFloat($('#chargeRate3').text());

    var dist_sys_rate = parseFloat($('#chargeRate4').text());
    var sup_sys_rate = parseFloat($('#chargeRate5').text());
    var mtrg_ret_cust_rate = parseFloat($('#chargeRate6').text());
    var mtrg_sys_rate = parseFloat($('#chargeRate7').text());

    var rfsc_rate = parseFloat($('#chargeRate8').text());
    var life_line_rate = parseFloat($('#chargeRate9').text());

    var npc_stranded_debts_rate = parseFloat($('#chargeRate15').text());
    var npc_sc_rate = parseFloat($('#chargeRate16').text());
    var me_rate = parseFloat($('#chargeRate17').text());
    var ucme_rate = parseFloat($('#chargeRate18').text());
    var fit_all_rate = parseFloat($('#chargeRate19').text());
    var environment_rate = parseFloat($('#chargeRate20').text());
    var sc_subsidy_rate = parseFloat($('#chargeRate21').text());
    var erc_case_rate = parseFloat($('#chargeRate22').text());


    var gen_sys_chg = total_kwh * gen_sys_rate;
    var trans_sys_chg = total_kwh * trans_sys_rate;
    var sys_loss_chg = total_kwh * sys_loss_rate;
    var chargeTotalAmount1 = gen_sys_chg + trans_sys_chg + sys_loss_chg;

    var dist_sys_chg = total_kwh * dist_sys_rate;
    var sup_sys_chg = total_kwh * sup_sys_rate;
    var mtrg_ret_cust_chg = total_kwh * mtrg_ret_cust_rate;
    var mtrg_sys_chg = total_kwh * mtrg_sys_rate;
    var chargeTotalAmount2 = dist_sys_chg + sup_sys_chg + mtrg_ret_cust_chg + mtrg_sys_chg;

    var rfsc_chg = total_kwh * rfsc_rate;
    var life_line_chg = total_kwh * life_line_rate;
    var chargeTotalAmount3 = rfsc_chg + sup_sys_chg + life_line_chg;

    var npc_stranded_debts_chg = total_kwh * npc_stranded_debts_rate;
    var npc_sc_chg = total_kwh * npc_sc_rate;
    var me_chg = total_kwh * me_rate;
    var ucme_chg = total_kwh * ucme_rate;
    var fit_all_chg = total_kwh * fit_all_rate;
    var environment_chg = total_kwh * environment_rate;
    var sc_subsidy_chg = total_kwh * sc_subsidy_rate;
    var erc_case_rchg = total_kwh * erc_case_rate;
    var chargeTotalAmount4 = npc_stranded_debts_chg + npc_sc_chg + me_chg + ucme_chg + fit_all_chg + environment_chg + sc_subsidy_chg + erc_case_rchg;

    var monthLyBill = chargeTotalAmount1 + chargeTotalAmount2 + chargeTotalAmount3 + chargeTotalAmount4;

    $('#total-energy').html(total_kwh.toFixed(2));
    $('#monthly-bill').html(monthly_bill.toFixed(2));
    
    $('#chargeAmount1').html(gen_sys_chg.toFixed(2));
    $('#chargeAmount2').html(trans_sys_chg.toFixed(2));
    $('#chargeAmount3').html(sys_loss_chg.toFixed(2));
    $('#chargeTotalAmount1').html(chargeTotalAmount1.toFixed(2));
    
    $('#chargeAmount4').html(dist_sys_chg.toFixed(2));
    $('#chargeAmount5').html(sup_sys_chg.toFixed(2));
    $('#chargeAmount6').html(mtrg_ret_cust_chg.toFixed(2));
    $('#chargeAmount7').html(mtrg_sys_chg.toFixed(2));
    $('#chargeTotalAmount2').html(chargeTotalAmount2.toFixed(2));
    
    $('#chargeAmount8').html(rfsc_chg.toFixed(2));
    $('#chargeAmount9').html(life_line_chg.toFixed(2));
    $('#chargeTotalAmount3').html(chargeTotalAmount3.toFixed(2));
    
    $('#chargeAmount15').html(npc_stranded_debts_chg.toFixed(2));
    $('#chargeAmount16').html(npc_sc_chg.toFixed(2));
    $('#chargeAmount17').html(me_chg.toFixed(2));
    $('#chargeAmount18').html(ucme_chg.toFixed(2));
    $('#chargeAmount19').html(fit_all_chg.toFixed(2));
    $('#chargeAmount20').html(environment_chg.toFixed(2));
    $('#chargeAmount21').html(sc_subsidy_chg.toFixed(2));
    $('#chargeAmount22').html(erc_case_rchg.toFixed(2));
    $('#chargeTotalAmount4').html(monthLyBill.toFixed(2));
} 
</script>

<?php include('layouts/footer-admin.php'); ?>