<?php
include('func/functions.php');

$user_id = $_SESSION['user']['id'];

$appliances_sql = 'SELECT * FROM user_appliances WHERE user_id=:user_id ORDER BY name ASC';
$appliances = get($dbConn, $appliances_sql, array(':user_id'=>$user_id));

$dates = array();

$get_start = date('Y-m-d', strtotime(trim($_GET['start'])));
$get_end = date('Y-m-d', strtotime(trim($_GET['end'])));

$start = new DateTime($get_start);
$end   = new DateTime($get_end);

for($i = $start; $i <= $end; $i->modify('+1 day')) {
	$dates[] = $i->format("Y-m-d");
}

$n = 0;
$data = array();
$data['days'] = [];
foreach ($appliances as $appliance) {
	$rate = 0;
	$last_rate = 0;
	$total_cost = 0;
	$wattage = intval($appliance->wattage);
	$hours = intval($appliance->hours);
	$kwh = ($wattage * $hours * 1) / 1000;
	$data['appliances'][$n]['name'] = $appliance->name;
	$counter_days = 0;
	$counter_data = 0;
	foreach ($dates as $date) {
		if(!in_array($date, $data['days'])) {
			$data['days'][$counter_days] = date('Y-m-d', strtotime($date));
			$counter_days++;
		}

		$rate_sql = 'SELECT * FROM daily_rate_per_hour WHERE date=:date ORDER BY date DESC';
		$row_rate = first($dbConn, $rate_sql, array(':date'=>$date));
		#var_dump($row_rate);
		#echo count($row_rate); echo '<br />';
		if($row_rate) {
			$rate = $row_rate['rate'];
			$last_rate = $rate;
		}
		$cost = $kwh * $rate;
		$data['appliances'][$n]['data'][$counter_data] = floatval(number_format($cost, 2));
		$counter_data++;
	}
	$n++;
}
#exit;
/*echo '<pre>';
print_r($data);
echo '</pre>';*/

json($data);