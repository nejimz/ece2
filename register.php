<?php 
include('func/functions_index.php');
$name = '';
$email = '';

$name_err = '';
$email_err = '';
$password_err = '';

if(isset($_POST['registration']))
{
    $name = trim($_POST['name']);
    $email = trim($_POST['email']);
    $password = trim($_POST['password']);
    $password_confirmation = trim($_POST['password_confirmation']);

    $sqlEmailCount = 'SELECT COUNT(id) AS cnt FROM users WHERE email=:email LIMIT 1';
    $countEmail = count_row($dbConn, $sqlEmailCount, array( ':email' => $email ));

    $n = 0;
    $errors = [];

    if ($name == '') {
        $errors[$n] = 'Name is required!';
        $n++;
    }
    if ($email == '') {
        $errors[$n] = 'E-Mail is required!';
        $n++;
    } elseif ($countEmail > 0) {
        $errors[$n] = 'E-Mail already exists!';
        $n++;
    }

    if ($password == '' || $password_confirmation == '') {
        $errors[$n] = 'Password does not matched!';
        $n++;
    } elseif ($password != $password_confirmation) {
        $errors[$n] = 'Password does not matched!';
        $n++;
    }

    if(count($errors) > 0) {
        $_SESSION['errors'] = $errors;
        header('Location: register.php');
        exit;
    }

    $dateNow = date('Y-m-d H:i:s');
    $password = md5($password);
    $sql = 'INSERT INTO users (name, email, password, role, created_at, updated_at) VALUES (:name, :email, :password, :role, :created_at, :updated_at)';
    $values = array(
        ':name' => $name, 
        ':email' => $email, 
        ':password' => $password, 
        ':role' => 1, 
        ':created_at' => $dateNow, 
        ':updated_at' => $dateNow
    );

    insert($dbConn, $sql, $values);
    $_SESSION['success'] = 'Account successfully created!';

    header('Location: register.php');
    exit;
}

include('layouts/header.php'); 
?>
    <div class="grid-container">
        <div class="item1"></div>
        <div class="item1">
            <div class="login-border">
                <h1 class="login-title">ECE</h1>
                <h4 class="login-title-sub">Electricity<br>Consumption<br>Estimator</h4><br>
                <?php include('layouts/validation-messages.php'); ?>
                <form class="login-form" method="POST" action="register.php">
                    <input id="name" type="text" name="name" placeholder="Name" value=""  autofocus><br><br>
                    <input id="email" type="email" name="email" placeholder="E-Mail" value="" ><br><br>
                    <input id="password" type="password" name="password" placeholder="Password" ><br><br>
                    <input id="password_confirmation" type="password" name="password_confirmation" placeholder="Password Confirmation" >
                    <br><br>
                    <button type="submit" class="btn success" name="registration">Register</button>
                </form>
                <p>Already have an account? <a href="/">Login</a></p>
            </div>
        </div>
        <div class="item1"></div>
    </div>
<?php include('layouts/footer.php'); ?>