
<?php
include('func/functions.php');

$sql = 'SELECT * FROM user_appliances WHERE `user_appliances`.deleted_at IS NULL AND user_id=:user_id ORDER BY name ASC;';
$values = array(':user_id'=>$_SESSION['user']['id']);
$rows = get($dbConn, $sql, $values);

$rateSql = 'SELECT * FROM rates_charges WHERE id=:id LIMIT 1';
$rate = first($dbConn, $rateSql, array(':id'=>1));

include('layouts/header-admin.php');
?>
<script src="js/highcharts.js"></script>
<style type="text/css">
.switch {
  position: relative;
  display: inline-block;
  width: 48px;
  height: 9px;
}

/* Hide default HTML checkbox */
.switch input {display:none;}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
#running_time {
    font-weight: bold;
}
</style>
<h1 class=""><i class="fa fa-tachometer"></i>&nbsp;Monthly Usage (<span id="total_amount">0.00</span>)</h1>
<br />

<?php
$switch = '';
$grand_total = 0;
$dateTimeNow = date('Y-m-d H:i:s');
foreach($rows as $row) {
    $inputChecked = '';
    $inputName = 'switcher' . $row->id;
    $image = 'images/appliances/no-image.jpg';
    if ($row->image != '') {
        $image = $row->image;
    }
    $usageValues = array(':user_id'=>$row->user_id, ':appliance_id'=>$row->id);
    #$usageSql = 'SELECT * FROM `usage` WHERE user_id=:user_id AND appliance_id=:appliance_id AND end_datetime IS NULL ORDER BY start_datetime DESC LIMIT 1';
    $usageSql = 'SELECT * FROM `usage` INNER JOIN user_appliances ON `usage`.appliance_id=user_appliances.id WHERE `usage`.user_id=:user_id AND appliance_id=:appliance_id ORDER BY user_appliances.id ASC, `usage`.start_datetime ASC';
    $usageRow = get($dbConn, $usageSql, $usageValues);
    $total = 0;
    $last_is_null = false;
    foreach ($usageRow as $row) {
      $start_usage = date('Y-m-d H:i:s', strtotime($row->start_datetime));
      $start_dateTime = new DateTime($start_usage);

      if (is_null($row->end_datetime)) {
        $end_dateTime = new DateTime($dateTimeNow);
      } else {
        $end_dateTime = new DateTime(date('Y-m-d H:i:s', strtotime($row->end_datetime)));
      }

      $last_is_null = is_null($row->end_datetime);

      $interval = $start_dateTime->diff($end_dateTime);
      $day = $interval->format('%d');
      $hour = $interval->format('%H');
      $running_time = $interval->format('%d days %H hrs %i mins %s sec');
      $days_to_hours = $day * 24;
      $number_of_hours = $days_to_hours + $hour;
      $kwh = ($row->wattage * $number_of_hours) / 1000;
      $amount = $rate['amount'] * $kwh;

      $total += $amount;
    }

    $grand_total += $total;

    if ($last_is_null) {
      $inputChecked = 'checked';
      $running_time = '00 days 00 hrs 00 mins 00 sec';
    }
    /*$startInterval = new DateTime($dateTimeNow);

    if ($usageRow) {
        if (is_null($usageRow['end_datetime'])) {
            $inputChecked = 'checked';
        }
      $startDateTime = date('Y-m-d H:i:s', strtotime($usageRow['start_datetime']));
      $startInterval = new DateTime($startDateTime);
    }
    $endInterval = new DateTime($dateTimeNow);
    $interval = $startInterval->diff($endInterval);
    $running_time = $interval->format('%d days %H hrs %i mins %s sec');*/
?>
<div class="row">
    <div class="col-20"><img src="<?php echo $image; ?>" width="150px"></div>
    <div class="col-50">
        <h2><?php echo $row->name; ?></h2>
        <label>Running Amount:</label><strong id="running_amount<?php echo $row->id; ?>"><?php echo number_format(0, 2); ?></strong><br>
        <label>Running Time:</label><strong id="running_time<?php echo $row->id; ?>"><?php echo $running_time; ?></strong>
        <br>
        <label class="switch">
            <input class="switcherClass" type="checkbox" id="<?php echo $inputName; ?>" value="<?php echo $row->id; ?>" onclick="switcher(<?php echo $row->id; ?>)" <?php echo $inputChecked; ?> >
            <span class="slider round"></span>
        </label>
    </div>
</div>
<?php
}
?>
<script type="text/javascript">
    var xhr_start = null;
    var xhr_end = null;
    var xhr_update = null;
    $('#total_amount').html('<?php echo number_format($grand_total, 2); ?>');
    $(function(){
      <?php #echo $switch; ?>
      setInterval(function(){
        switcherUpdate();
      }, 1000);
    });

    function switcher(appliance_id) {
 /*       var status = $('#switcher' + appliance_id).is(':checked');
        var value = $('#switcher' + appliance_id).val();
        console.log(status);
        if (status) {
            console.log(value);
            // timer start
            var xhr_start = $.ajax({
                type : "post",
                url : "usage-start.php",
                cache : false,
                data: 'appliance_id=' + appliance_id,
                dataType : "json",
                beforeSend: function(xhr){
                    if (xhr_start != null){xhr_start.abort();}
                }
            }).done(function(json){
                console.log(json['datetime']);
            }).fail(function(jqXHR, textStatus){
            });
        }*/
      var status = $('#switcher' + appliance_id).is(':checked');
      var value = $('#switcher' + appliance_id).val();
      console.log(status);
      if (status) {
        // timer start
        var xhr_start = $.ajax({
            type : "post",
            url : "usage-start.php",
            cache : false,
            data: 'appliance_id=' + appliance_id,
            dataType : "json",
            beforeSend: function(xhr){
                if (xhr_start != null){xhr_start.abort();}
            }
        }).done(function(json){
            console.log(json);
            $('total_amount').html(json['total_amount']);
        }).fail(function(jqXHR, textStatus){
        });
      } else {
        // timer end
        var xhr_end = $.ajax({
            type : "post",
            url : "usage-end.php",
            cache : false,
            data: 'appliance_id=' + appliance_id,
            dataType : "json",
            beforeSend: function(xhr){
                if (xhr_end != null){xhr_end.abort();}
            }
        }).done(function(json){
          $('#running_time' + json['appliance_id']).html(json['running_time']);
            $('total_amount').html(json['total_amount']);
        }).fail(function(jqXHR, textStatus){
        });
      }
    }

    function switcherUpdate() {
      //var checkbox = $('input[type=checkbox]').is(':checked');
      var checkboxes = $('.switcherClass:checkbox:checked');
      var appliances_id = [];
      $.each(checkboxes, function(key, value) {
        appliances_id[key] = value['value'];
      });
      //console.log(appliances_id);
      var xhr_update = $.ajax({
          type : "post",
          url : "usage-update.php",
          cache : false,
          data: 'appliances_id=' + JSON.stringify(appliances_id),
          dataType : "json",
          beforeSend: function(xhr){
            if(xhr_update != null){xhr_update.abort();}
          }
      }).done(function(json){
        $.each(json['appliance'], function(key, value) {
          $('#running_time' + value['id']).html(value['running_time']);
          $('#running_amount' + value['id']).html(value['running_amount']);
        });
        $('#total_amount').html(json['total_amount']);
      }).fail(function(jqXHR, textStatus){
      });
    }
</script>
<?php include('layouts/footer-admin.php'); ?>
