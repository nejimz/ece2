
<?php
#include('func/session-global-filter.php');
#require_once('func/db-connection.php');
include('func/functions.php');
$user_id = $_SESSION['user']['id'];
$route = 'create';
$name = '';
$email = '';
$role = '';

if (isset($_POST['submit'])) {
    $n = 0;
    $errors = [];
    $submit = trim($_POST['submit']);
    $name = trim($_POST['name']);
    $email = trim($_POST['email']);
    $role = trim($_POST['role']);
    $now = date('Y-m-d H:i:s');

    if($name == '')
    {  
        $n++;
        $errors[$n] = 'Name is required!';
    }
    if ($email == '') {
        $n++;
        $errors[$n] = 'E-Mail is required!';
    }
    if ($role == '') {
        $n++;
        $errors[$n] = 'role is required!';
    }

    if ($submit == 'create') {
        $sql = 'SELECT COUNT(id) cnt FROM users WHERE email=:email LIMIT 1';
        $count = count_row($dbConn, $sql, array(':email'=>$email));

        if($count > 0) {
            $n++;
            $errors[$n] = 'E-Mail already exists!';
        }

        if(count($errors) > 0) {
            #$_SESSION['errors'] = $errors;
            #header('Location: users-create.php');
            #exit;
            redirect('users-create.php?errors=' . json_encode($errors));
        } else {
            $sql = 'INSERT INTO users (name, email, password, role, created_at, updated_at) VALUES (:name, :email, \'\', :role, :created_at, :updated_at)';
            $values = array(':name'=>$name, ':email'=>$email, ':role'=>$role, ':created_at'=>$now, ':updated_at'=>$now);
            insert($dbConn, $sql, $values);

            #$_SESSION['success'] = 'User successfully added!';
            #header('Location: users-create.php');
            #exit;
            redirect('users-create.php?success=User successfully added!');
        }
    } elseif ($submit == 'update') {
        $id = trim($_GET['id']);
        $sql = 'SELECT COUNT(id) cnt FROM users WHERE email=:email AND id<>:id LIMIT 1';
        $count = count_row($dbConn, $sql, array(':email'=>$email, ':id'=>$id));

        if($count > 0) {
            $n++;
            $errors[$n] = 'E-Mail already exists!';
        }

        if(count($errors) > 0) {
            #$_SESSION['errors'] = $errors;
            #header('Location: users-create.php?id=' . $id);
            #exit;
            redirect('users-create.php?id=' . $id . '&errors=' . json_encode($errors));
        } else {
            $sql = 'UPDATE users SET name=:name, email=:email, role=:role, updated_at=:updated_at WHERE id=:id';
            $values = array(':name'=>$name, ':email'=>$email, ':role'=>$role, ':updated_at'=>$now, ':id'=>$id);
            insert($dbConn, $sql, $values);

            #$_SESSION['success'] = 'User successfully updated!';
            #header('Location: users-create.php?id=' . $id);
            #exit;
            redirect('users-create.php?id=' . $id . '&success=User successfully updated!');
        }
    }
}

if(isset($_GET['id'])) {
    $id = trim($_GET['id']);
    $sql = 'SELECT * FROM users WHERE id=:id ORDER BY name DESC LIMIT 1';
    $row = first($dbConn, $sql, array(':id'=>$id));

    $route = 'update';
    $name = $row['name'];
    $email = $row['email'];
    $role = $row['role'];
}

include('layouts/header-admin.php');
?>
<h1 class="title is-3"><i class="fa fa-users"></i>&nbsp;Users <?php echo ucwords($route); ?></h1>
<br><br>
<?php include('layouts/validation-messages.php'); ?>
<form action="users-create.php<?php echo (isset($id))? '?id=' . $id : ''; ?>" method="post">

    <div class="row">
        <div class="col-10">
            <label for="name">Name</label>
        </div>
        <div class="col-25">
            <input type="text" name="name" id="name" value="<?php echo $name; ?>">
        </div>
    </div>

    <div class="row">
        <div class="col-10">
            <label for="email">E-Mail</label>
        </div>
        <div class="col-25">
            <input type="text" name="email" id="email" value="<?php echo $email; ?>">
        </div>
    </div>

    <div class="row">
        <div class="col-10">
            <label for="role">Role</label>
        </div>
        <div class="col-25">
            <select name="role" id="role">
                <option value="1">Normal</option>
                <option value="0">Admin</option>
            </select>
        </div>
    </div>
    <?php
    if($route == 'update') {
    ?>
    <div class="row">
        <div class="col-10">
            <label for="password">Password</label>
        </div>
        <div class="col-20">
            <input type="text" name="password" id="password" value="" readonly="">
        </div>
        <div class="col-5">
            <button onclick="generate_password(<?php echo $id; ?>)" type="button" class="btn info"><i class="fa fa-refresh"></i></button>
        </div>
    </div>
    <?php
    }
    ?>
    <div class="row">
        <div class="col-10"></div>
        <div class="col-25">
            <button class="btn success" name="submit" value="<?php echo $route; ?>">Submit</button>
        </div>
    </div>
</form>

<script type="text/javascript">
    var xhr_generate_password = null;

    $('#role option[value=<?php echo ($role == 0)? 0 : 1; ?>]').attr('selected','selected');

    function generate_password(id)
    {
        xhr_generate_password = $.ajax({
            type : 'get',
            url : 'generate-password.php',
            data : 'id=' + id,
            dataType : "json",
            beforeSend: function(xhr) 
            {
                if (xhr_generate_password != null)
                {
                    xhr_generate_password.abort();
                }
            }
        }).done(function(data) 
        {
            $('#password').val(data['password']);
        }).fail(function(jqXHR, textStatus) 
        {
            //console.log('Request failed: ' + textStatus);
        });
    }
</script>
<?php include('layouts/footer-admin.php'); ?>