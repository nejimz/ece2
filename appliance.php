
<?php
include('func/functions.php');

$search = '';

$sql = 'SELECT * FROM appliances ';
$values = array();
if(isset($_GET['search']))
{
    $search = trim($_GET['search']);
    $sql .= 'WHERE name LIKE :name ';
    $values[':name'] = "%$search%";
}

$sql .= 'ORDER BY name ASC LIMIT 50';

$rows = get($dbConn, $sql, $values);

include('layouts/header-admin.php');
?>
<h1 class="title is-3"><i class="fa fa-file"></i>&nbsp;Advisories</h1>
<form action="" method="get">
    <div class="row">
        <div class="col-15">
            <input class="input is-normal" type="text" name="search" id="search" value="<?php echo $search; ?>">
        </div>
        <div class="col-15">
            <button class="btn success"><i class="fa fa-search fa-lg"></i>Search</button>
        </div>
    </div>
</form>
<table>
    <thead>
        <tr>
            <th width="5%" class="text-center"><a href="appliance-create.php"><i class="fa fa-plus-circle fa-lg"></i></a></th>
            <th width="10%" class="text-center">Wattage</th>
            <th width="85%">Name</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($rows as $row) { ?>
        <tr>
            <td class="text-center">
            <?php
            if($_SESSION['user']['role'] == 0) {
            ?>
            <a href="appliance-create.php?id=<?php echo $row->id; ?>" title="Edit Appliance Details">
                <i class="fa fa-edit fa-lg"></i>
            </a>
            <?php
            } else {
            ?>
            <i class="fa fa-edit fa-lg"></i>
            <?php
            }
            ?>
            </td>
            <td class="text-center"><?php echo $row->wattage; ?></td>
            <td><?php echo $row->name; ?></td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<?php include('layouts/footer-admin.php'); ?>