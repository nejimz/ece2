
<?php
#include('func/session-global-filter.php');
#require_once('func/db-connection.php');
include('func/functions.php');
$user_id = $_SESSION['user']['id'];
$route = 'create';
$name = '';
$date = '';
$rate = '';

if (isset($_POST['submit'])) {
    $n = 0;
    $errors = [];
    $submit = trim($_POST['submit']);
    $name = trim($_POST['name']);
    $date = date('Y-m-01', strtotime(trim($_POST['date'])));
    $rate = trim($_POST['rate']);
    $now = date('Y-m-d H:i:s');

    if($name == '')
    {  
        $errors[$n] = 'Date is required!';
        $n++;
    }
    if($date == '')
    {  
        $errors[$n] = 'Date is required!';
        $n++;
    }
    if ($rate == '') {
        $errors[$n] = 'Rate is required!';
        $n++;
    }
    if (!is_numeric($rate)) {
        $errors[$n] = 'Rate must be a number!';
        $n++;
    }

    if ($submit == 'create') {
        $sqlName = 'SELECT COUNT(id) cnt FROM rates_charges WHERE name=:name LIMIT 1';
        $countName = count_row($dbConn, $sqlName, array(':name'=>$name));

        if($countName > 0) {
            $errors[$n] = 'Name already exists!';
            $n++;
        }
        $sql = 'SELECT COUNT(id) cnt FROM rates_charges WHERE YEAR(`date`)=:year AND MONTH(`date`)=:month LIMIT 1';
        $count = count_row($dbConn, $sql, array(':year'=>date('Y', strtotime($date)), ':month'=>date('m', strtotime($date))));

        if($count > 0) {
            $errors[$n] = 'Date already exists!';
            $n++;
        }

        if(count($errors) > 0) {
            #$_SESSION['errors'] = $errors;
            #header('Location: rate-charges-create.php');
            #exit;
            redirect('rate-charges-create.php?errors=' . json_encode($errors));
        }

        $sql = 'INSERT INTO rates_charges (name, `date`, amount, created_at) VALUES (:name, :date, :amount, :created_at)';
        $values = array(':name'=>$name, ':date'=>$date, ':amount'=>$rate, ':created_at'=>$now);
        insert($dbConn, $sql, $values);

        #$_SESSION['success'] = 'Rate successfully added!';
        #header('Location: rate-charges-create.php');
        #exit;
        redirect('rate-charges-create.php?success=Rate successfully added!');
    } elseif ($submit == 'update') {
        $id = trim($_GET['id']);

        $sqlName = 'SELECT COUNT(id) cnt FROM rates_charges WHERE name=:name AND id<>:id LIMIT 1';
        $countName = count_row($dbConn, $sqlName, array(':name'=>$name, ':id'=>$id));

        if($countName > 0) {
            $errors[$n] = 'Name already exists!';
            $n++;
        }

        /*$sql = 'SELECT COUNT(id) cnt FROM rates_charges WHERE YEAR(`date`)=:year AND MONTH(`date`)=:month AND id<>:id LIMIT 1';
        $count = count_row($dbConn, $sql, array(':year'=>date('Y', strtotime($date)), ':month'=>date('m', strtotime($date)), ':id'=>$id));

        if($count > 0) {
            $errors[$n] = 'Date already exists!';
            $n++;
        }*/

        if(count($errors) > 0) {
            #$_SESSION['errors'] = $errors;
            #header('Location: rate-charges-create.php?id=' . $id);
            #exit;
            redirect('rate-charges-create.php?id=' . $id . '&errors=' . json_encode($errors));
        }

        $sql = 'UPDATE rates_charges SET name=:name, date=:date, amount=:amount WHERE id=:id';
        $values = array(':name'=>$name, ':date'=>$date, ':amount'=>$rate, ':id'=>$id);
        insert($dbConn, $sql, $values);

        #$_SESSION['success'] = 'Rate successfully updated!';
        #header('Location: rate-charges-create.php?id=' . $id);
        #exit;
        redirect('rate-charges-create.php?id=' . $id . '&success=Rate successfully updated!');
    }
}

if(isset($_GET['id'])) {
    $id = trim($_GET['id']);
    $sql = 'SELECT * FROM rates_charges WHERE id=:id ORDER BY created_at DESC LIMIT 1';
    $row = first($dbConn, $sql, array(':id'=>$id));

    $route = 'update';
    $name = $row['name'];
    $date = $row['date'];
    $rate = $row['amount'];
}

include('layouts/header-admin.php');
?>
<h1 class="title is-3"><i class="fa fa-users"></i>&nbsp;Rate Charges <?php echo ucwords($route); ?></h1>
<br><br>
<?php include('layouts/validation-messages.php'); ?>
        
<form action="rate-charges-create.php<?php echo (isset($id))? '?id=' . $id : ''; ?>" method="post">

    <div class="row">
        <div class="col-10">
            <label for="name">Name</label>
        </div>
        <div class="col-25">
            <input type="text" name="name" id="name" value="<?php echo $name; ?>">
        </div>
    </div>

    <div class="row">
        <div class="col-10">
            <label for="date">Date</label>
        </div>
        <div class="col-25">
            <input type="date" name="date" id="date" value="<?php echo $date; ?>">
        </div>
    </div>

    <div class="row">
        <div class="col-10">
            <label for="rate">Rate</label>
        </div>
        <div class="col-25">
            <input type="text" name="rate" id="rate" value="<?php echo $rate; ?>">
        </div>
    </div>

    <div class="row">
        <div class="col-10"></div>
        <div class="col-25">
            <button class="btn success" name="submit" value="<?php echo $route; ?>">Submit</button>
        </div>
    </div>

</form>

<?php include('layouts/footer-admin.php'); ?>