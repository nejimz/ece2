<?php
include('../func/mobile-auth-check.php');

$n = 0;
$errors = array();
$name = trim($_POST['name']);
$wattage = trim($_POST['wattage']);

if($name == '') {  
    $errors[$n] = 'Name is required!';
    $n++;
}
if ($wattage == '') {
    $errors[$n] = 'Wattage is required!';
    $n++;
}

if(count($errors) > 0) {
    $_SESSION['errors'] = $errors;
    json(array('errors' => $errors));
}

$dateNow = date('Y-m-d H:i:s');
$sql = 'INSERT INTO appliances (name, wattage) VALUES (:name, :wattage)';
$values = array(':name'=>$name, ':wattage'=>$wattage);
insert($dbConn, $sql, $values);
json(array('Appliances successfully added!'));
