<?php
include('../func/mobile-auth-check.php');

$n = 0;
$errors = array();
$id = trim($_POST['id']);
$name = trim($_POST['name']);
$date = date('Y-m-d', strtotime(trim($_POST['date'])));
$rate = trim($_POST['rate']);
$now = date('Y-m-d H:i:s');

if($name == '')
{  
    $n++;
    $errors[$n] = 'Name is required!';
}
if($date == '')
{  
    $n++;
    $errors[$n] = 'Date is required!';
}
if ($rate == '') {
    $n++;
    $errors[$n] = 'Rate is required!';
}
if (!is_numeric($rate)) {
    $n++;
    $errors[$n] = 'Rate must be a number!';
}

$sql = 'SELECT COUNT(id) cnt FROM rates_charges WHERE YEAR(`date`)=:year AND MONTH(`date`)=:month AND id<>:id LIMIT 1';
$count = count_row($dbConn, $sql, array(':year'=>date('Y', strtotime($date)), ':month'=>date('m', strtotime($date))));

if($count > 0) {
    $n++;
    $errors[$n] = 'Date already exists!';
}

if(count($errors) > 0) {
    $_SESSION['errors'] = $errors;
    json(array('errors' => $errors));
}

$sql = 'UPDATE rates_charges SET name=:name, date=:date, rate=:rate WHERE id=:id';
$values = array(':name'=>$name, ':date'=>$date, ':rate'=>$rate, ':id'=>$id);
insert($dbConn, $sql, $values);
json(array('Rate Charges successfully updated!'));
