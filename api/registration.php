<?php
include('../func/functions_index.php');

$name = trim($_POST['name']);
$email = trim($_POST['email']);
$password = trim($_POST['password']);
$password_confirmation = trim($_POST['password_confirmation']);

$sqlEmailCount = 'SELECT COUNT(id) AS cnt FROM users WHERE email=:email LIMIT 1';
$countEmail = count_row($dbConn, $sqlEmailCount, array( ':email' => $email ));

$n = 0;
$errors = [];

if ($name == '') {
    $errors[$n] = 'Name is required!';
    $n++;
}

if ($email == '') {
    $errors[$n] = 'E-Mail is required!';
    $n++;
} elseif ($countEmail > 0) {
    $errors[$n] = 'E-Mail already exists!';
    $n++;
}

if ($password == '' || $password_confirmation == '') {
    $errors[$n] = 'Password is required!';
    $n++;
} elseif ($password != $password_confirmation) {
    $errors[$n] = 'Password does not matched!';
    $n++;
}

if(count($errors) > 0) {
    json(array('errors' => $errors));
}

$dateNow = date('Y-m-d H:i:s');
$password = md5($password);
$sql = 'INSERT INTO users (name, email, password, role, created_at, updated_at) VALUES (:name, :email, :password, :role, :created_at, :updated_at)';
$values = array(
    ':name' => $name, 
    ':email' => $email, 
    ':password' => $password, 
    ':role' => 1, 
    ':created_at' => $dateNow, 
    ':updated_at' => $dateNow
);

insert($dbConn, $sql, $values);
json(array('Account successfully created!'));