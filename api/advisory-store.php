<?php
include('../func/mobile-auth-check.php');

$n = 0;
$errors = array();
$title = trim($_POST['title']);
$description = trim($_POST['description']);

if($title == '') {  
    $errors[$n] = 'Title is required!';
    $n++;
}
if ($description == '') {
    $errors[$n] = 'Description is required!';
    $n++;
}

if(count($errors) > 0) {
    $_SESSION['errors'] = $errors;
    json(array('errors' => $errors));
}

$dateNow = date('Y-m-d H:i:s');
$sql = 'INSERT INTO advisories (user_id, title, description, created_at, updated_at) VALUES (:user_id, :title, :description, :created_at, :updated_at)';
$values = array(':user_id'=>$user_id, ':title'=>$title, ':description'=>$description, ':created_at'=>$dateNow, ':updated_at'=>$dateNow);
insert($dbConn, $sql, $values);
json(array('Advisories successfully added!'));
