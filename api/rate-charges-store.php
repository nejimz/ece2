<?php
include('../func/mobile-auth-check.php');

$n = 0;
$errors = array();
$date = date('Y-m-d', strtotime(trim($_POST['date'])));
$rate = trim($_POST['rate']);
$now = date('Y-m-d H:i:s');

if($date == '')
{  
    $n++;
    $errors[$n] = 'Date is required!';
}
if ($rate == '') {
    $n++;
    $errors[$n] = 'Rate is required!';
}
if (!is_numeric($rate)) {
    $n++;
    $errors[$n] = 'Rate must be a number!';
}

$sql = 'SELECT COUNT(id) cnt FROM rates_charges WHERE YEAR(`date`)=:year AND MONTH(`date`)=:month LIMIT 1';
$count = count_row($dbConn, $sql, array(':year'=>date('Y', strtotime($date)), ':month'=>date('m', strtotime($date))));

if($count > 0) {
    $n++;
    $errors[$n] = 'Date already exists!';
}

if(count($errors) > 0) {
    $_SESSION['errors'] = $errors;
    json(array('errors' => $errors));
}

$sql = 'INSERT INTO rates_charges (`date`, rate, created_at, updated_at) VALUES (:date, :rate, :created_at, :updated_at)';
$values = array(':date'=>$date, ':rate'=>$rate, ':created_at'=>$now, ':updated_at'=>$now);
insert($dbConn, $sql, $values);

json(array('Rate Charges successfully added!'));
