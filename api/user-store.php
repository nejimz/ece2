<?php
include('../func/mobile-auth-check.php');

$n = 0;
$errors = array();
$name = trim($_POST['name']);
$email = trim($_POST['email']);
$role = trim($_POST['role']);
$now = date('Y-m-d H:i:s');

if($name == '')
{  
    $errors[$n] = 'Name is required!';
    $n++;
}
if ($email == '') {
    $errors[$n] = 'E-Mail is required!';
    $n++;
}
if ($role == '') {
    $errors[$n] = 'role is required!';
    $n++;
}

$sql = 'SELECT COUNT(id) cnt FROM users WHERE email=:email LIMIT 1';
$count = count_row($dbConn, $sql, array(':email'=>$email));

if($count > 0) {
    $errors[$n] = 'E-Mail already exists!';
    $n++;
}

if(count($errors) > 0) {
	json($errors);
}

$sql = 'INSERT INTO users (name, email, password, role, created_at, updated_at) VALUES (:name, :email, \'\', :role, :created_at, :updated_at)';
$values = array(':name'=>$name, ':email'=>$email, ':role'=>$role, ':created_at'=>$now, ':updated_at'=>$now);
insert($dbConn, $sql, $values);
json('User successfully added!');