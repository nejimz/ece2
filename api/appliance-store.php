<?php
include('../func/mobile-auth-check.php');

$n = 0;
$errors = array();
$name = trim($_POST['name']);
$wattage = trim($_POST['wattage']);

if($name == '') {  
    $errors[$n] = 'Name is required!';
    $n++;
}
if ($wattage == '') {
    $errors[$n] = 'Wattage is required!';
    $n++;
}

$sql = 'SELECT COUNT(id) cnt FROM user_appliances WHERE name=:name LIMIT 1';
$count = count_row($dbConn, $sql, array(':name'=>$name));

if($count > 0) {
    $errors[$n] = 'Name already exists!';
    $n++;
}

if(count($errors) > 0) {
    json(array('errors' => $errors));
}

$sql = 'INSERT INTO user_appliances (user_id, name, wattage) VALUES (:user_id, :name, :wattage)';
$values = array(':user_id'=>$user_id, ':name'=>$name, ':wattage'=>$wattage);
insert($dbConn, $sql, $values);
json(array('Appliance successfully added!'));
