<?php
include('../func/mobile-auth-check.php');

$n = 0;
$errors = array();
$id = trim($_POST['id']);
$title = trim($_POST['title']);
$description = trim($_POST['description']);

if($title == '') {  
    $errors[$n] = 'Title is required!';
    $n++;
}
if ($description == '') {
    $errors[$n] = 'Description is required!';
    $n++;
}

if(count($errors) > 0) {
    $_SESSION['errors'] = $errors;
    json(array('errors' => $errors));
}

$dateNow = date('Y-m-d H:i:s');
$sql = 'UPDATE advisories SET user_id=:user_id, title=:title, description=:description, updated_at=:updated_at WHERE id=:id';
$values = array(':user_id'=>$user_id, ':title'=>$title, ':description'=>$description, ':id'=>$id, ':updated_at'=>$dateNow);
insert($dbConn, $sql, $values);
json(array('Advisories successfully updated!'));
