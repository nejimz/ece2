<?php
include('../func/mobile-auth-check.php');

$sqlUA = 'SELECT * FROM user_appliances WHERE deleted_at IS NULL AND user_id=:user_id ORDER BY name ASC';
$rows = get($dbConn, $sqlUA, array($user_id));
$appliances = toArray($rows);
json($appliances);