<?php
include('../func/functions_index.php');

if(isset($_POST['email']))
{
    $email = trim($_POST['email']);
    $password = trim($_POST['password']);
    $password = md5($password);

    $sql = 'SELECT * FROM users WHERE email=:email AND password=:password LIMIT 1';
    $user = first($dbConn, $sql, array( ':email' => $email, ':password' => $password ));

    if($user) {
        $orpb = openssl_random_pseudo_bytes(16);
        $token = bin2hex($orpb);
        $data = array('token' => $token, 'user_id' => $user['id'], 'role' => $user['role']);

        $sqlToken = 'INSERT INTO mobile_auth (token, user_id, role) VALUES (:token, :user_id, :role)';
        insert($dbConn, $sqlToken, $data);
		$data['name'] = $user['name'];
        //json($data);
        json($token);
        exit;
    } else {
        $errors = array(
            'error' => 'Invalid credentials!'
        );
        //json($errors);
        json(0);
        exit;
    }
}
json(['404']);
exit;