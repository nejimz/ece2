<?php
include('../func/mobile-auth-check.php');

$n = 0;
$errors = array();
$id = trim($_POST['id']);
$name = trim($_POST['name']);
$email = trim($_POST['email']);
$role = trim($_POST['role']);
$now = date('Y-m-d H:i:s');

if($name == '')
{  
    $errors[$n] = 'Name is required!';
    $n++;
}
if ($email == '') {
    $errors[$n] = 'E-Mail is required!';
    $n++;
}
if ($role == '') {
    $errors[$n] = 'role is required!';
    $n++;
}

$sqlC = 'SELECT COUNT(id) cnt FROM users WHERE email=:email AND id<>:id LIMIT 1';
$count = count_row($dbConn, $sqlC, array(':email'=>$email, ':id'=>$id));

if($count > 0) {
    $errors[$n] = 'E-Mail already exists!';
    $n++;
}

if(count($errors) > 0) {
    json($errors);
}

$sqlU = 'UPDATE users SET name=:name, email=:email, role=:role, updated_at=:updated_at WHERE id=:id';
$values = array(':name'=>$name, ':email'=>$email, ':role'=>$role, ':updated_at'=>$now, ':id'=>$id);
insert($dbConn, $sqlU, $values);
json('User successfully updated!');
