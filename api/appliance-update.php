<?php
include('../func/mobile-auth-check.php');

$n = 0;
$id = trim($_POST['id']);
$name = trim($_POST['name']);
$wattage = trim($_POST['wattage']);

if($name == '') {  
    $errors[$n] = 'Name is required!';
    $n++;
}
if ($wattage == '') {
    $errors[$n] = 'Wattage is required!';
    $n++;
}

$sql = 'SELECT COUNT(id) cnt FROM user_appliances WHERE name=:name AND id<>:id LIMIT 1';
$count = count_row($dbConn, $sql, array(':name'=>$name, ':id'=>$id));

if($count > 0) {
    $errors[$n] = 'Name already exists!';
    $n++;
}

if(count($errors) > 0) {
    json(array('errors' => $errors));
}

$sql = 'UPDATE user_appliances SET user_id=:user_id, name=:name, wattage=:wattage WHERE id=:id';
$values = array(':user_id'=>$user_id, ':name'=>$name, ':wattage'=>$wattage, ':id'=>$id);
insert($dbConn, $sql, $values);
json(array('Appliance successfully updated!'));