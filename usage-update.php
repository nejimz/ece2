<?php
include('func/functions.php');

if (isset($_POST['appliances_id'])) {
	$user_id = $_SESSION['user']['id'];
	$counter = 0;
	$data = array();
	$appliances_id = json_decode($_POST['appliances_id']);
	#print_r($appliances_id);
	$dateTimeNow = date('Y-m-d H:i:s');
	$grand_total = 0;

	$rateSql = 'SELECT * FROM rates_charges WHERE id=:id LIMIT 1';
	$rate = first($dbConn, $rateSql, array(':id'=>1));


	foreach ($appliances_id as $id) {
		#$sql = 'SELECT `usage`.id, start_datetime, wattage FROM `usage` INNER JOIN user_appliances ON `usage`.appliance_id=user_appliances.id WHERE `usage`.user_id=:user_id AND `usage`.appliance_id=:appliance_id AND `usage`.end_datetime IS NULL ORDER BY `usage`.start_datetime DESC LIMIT 1';
		$sql = 'SELECT `usage`.id, start_datetime, end_datetime, wattage FROM `usage` INNER JOIN user_appliances ON `usage`.appliance_id=user_appliances.id WHERE `user_appliances`.deleted_at IS NULL AND `usage`.user_id=:user_id AND `usage`.appliance_id=:appliance_id ORDER BY user_appliances.id ASC, `usage`.start_datetime ASC';
		$values = array(':user_id'=>$user_id, ':appliance_id'=>$id);
		$usage = get($dbConn, $sql, $values);
		$last_start_datetime = '';
		$last_end_datetime = '';
		$total = 0;

		$last_is_null = false;
		$id_list = array();
		foreach ($usage as $row) {
			$start_usage = date('Y-m-d H:i:s', strtotime($row->start_datetime));
			$start_dateTime = new DateTime($start_usage);

			if (is_null($row->end_datetime)) {
				$end_dateTime = new DateTime($dateTimeNow);
			} else {
				$end_dateTime = new DateTime(date('Y-m-d H:i:s', strtotime($row->end_datetime)));
			}

			$last_is_null = is_null($row->end_datetime);

			#$last_start_datetime = $start_datetime;
			#$last_end_datetime = $end_datetime;

		    $interval = $start_dateTime->diff($end_dateTime);
		    $day = $interval->format('%d');
		    $hour = $interval->format('%H');
		    $minute = $interval->format('%i');
		    $second = $interval->format('%s');
		    #$running_time = "$day days $hour hrs $minute mins $second sec";
		    $running_time = $interval->format('%d days %H hrs %i mins %s sec');
		    $days_to_hours = $day * 24;
		    $number_of_hours = $days_to_hours + $hour;
		    $kwh = ($row->wattage * $number_of_hours) / 1000;
		    $amount = $rate['amount'] * $kwh;

	    	$total += $amount;

	    	#$data['appliance'][$counter] = array('id'=>$id, 'start'=>$usage['start_datetime'], 
	    	#	'datetime'=>$dateTimeNow, 'running_time'=>$running_time, 'running_amount'=>number_format($amount, 2));
	    	#s$data['appliance'][$id][$row->id] = array('id'=>$id, 'start'=>$start_dateTime, 'end'=>$end_dateTime, 'datetime'=>$dateTimeNow, 'running_time'=>$running_time);
	    	$id_list = array('id'=>$id, 'start'=>$start_dateTime, 'end'=>$end_dateTime, 'datetime'=>$dateTimeNow, 'running_time'=>$running_time);
		}
	    #$data['appliance'][$id][$row->id] = array('id'=>$id, 'start'=>$start_dateTime, 'end'=>$end_dateTime, 'datetime'=>$dateTimeNow, 'running_time'=>$running_time);
	    $data['appliance'][$id] = $id_list;
	    $data['appliance'][$id]['running_amount'] = number_format($total, 2);
	    $grand_total += $total;
	    $counter++;
	}
	$data['total_amount'] = number_format($grand_total, 2);

	json($data);
}