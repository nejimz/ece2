<?php
include('func/functions.php');

if (isset($_POST['appliance_id'])) {

	$user_id = $_SESSION['user']['id'];
	$appliance_id = trim($_POST['appliance_id']);
	$dateTimeNow = date('Y-m-d H:i:s');

	$sql = 'INSERT INTO `usage` (user_id, appliance_id, start_datetime) VALUES (:user_id, :appliance_id, :start_datetime)';
	$values = array(':user_id'=>$user_id, ':appliance_id'=>$appliance_id, ':start_datetime'=>$dateTimeNow);
	#echo $user_id . '<br />' . $appliance_id;
	#print_r($values);
	#exit;
	insert($dbConn, $sql, $values);

	$data = array('datetime' => $dateTimeNow, 'total_amount'=>number_format(0, 2));

	json($data);
}