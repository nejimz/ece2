
<?php
#include('func/session-global-filter.php');
#require_once('func/db-connection.php');
include('func/functions.php');

$search = '';

$sql = 'SELECT * FROM users ';
$values = array();
if(isset($_GET['search']))
{
    $search = trim($_GET['search']);
    $sql .= 'WHERE name LIKE :name OR email LIKE :email ';
    $values[':name'] = "%$search%";
    $values[':email'] = "%$search%";
}

$sql .= 'ORDER BY name DESC';

$rows = get($dbConn, $sql, $values);

include('layouts/header-admin.php');
?>
<h1 class="title is-3"><i class="fa fa-users"></i>&nbsp;Users</h1>
<form action="" method="get">
    <div class="row">
        <div class="col-15">
            <input class="input is-normal" type="text" name="search" id="search" value="<?php echo $search; ?>">
        </div>
        <div class="col-15">
            <button class="btn success"><i class="fa fa-search fa-lg"></i>Search</button>
        </div>
    </div>
</form>

<table>
    <thead>
        <tr>
            <th width="5%" class="text-center"><a href="users-create.php"><i class="fa fa-plus-circle fa-lg"></i></a></th>
            <th width="35%">Name</th>
            <th width="30%">Email</th>
            <th width="10%" class="text-center">Access</th>
            <th width="20%">Created At</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($rows as $row) { ?>
        <tr>
            <td class="text-center">
                <a href="users-create.php?id=<?php echo $row->id; ?>" title="Edit User Details">
                    <i class="fa fa-edit fa-lg"></i>
                </a>
            </td>
            <td><?php echo $row->name; ?></td>
            <td><?php echo $row->email; ?></td>
            <td class="text-center"><?php echo role($row->role); ?></td>
            <td><?php echo $row->created_at; ?></td>
        </tr>
        <?php } ?>
    </tbody>
</table>

<?php include('layouts/footer-admin.php'); ?>