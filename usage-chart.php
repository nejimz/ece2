
<?php
include('func/functions.php');

$start = date("Y-m-01");
$end = date("Y-m-t");

include('layouts/header-admin.php');
?>
<script src="js/highcharts.js"></script>
<h1 class=""><i class="fa fa-tachometer"></i>&nbsp;Monthly Usage</h1>
<form id="monthly_usage" action="{{ route('usage.data') }}" method="get">
    <div class="row">
        <div class="col-15">
            <input class="input is-large" type="date" id="start" name="start" class="input" >
        </div>
        <div class="col-15">
            <input class="input is-large" type="date" id="end" name="end" class="input" >
        </div>
        <div class="col-10">
            <button class="btn success" type="submit">Submit</button>
        </div>
        <div class="col-70"></div>
    </div>
</form>
<br />
<div id="chart"></div>

<script type="text/javascript">
    var chart;
    var xhr_chart = null;
    var start = '<?php echo $start; ?>';
    var end = '<?php echo $end; ?>';
    chartData(start, end);

    $(function(){

        $('#monthly_usage').submit(function(){
            var start = $('#start').val();
            var end = $('#end').val();
            chartData(start, end);
            return false;
        });
    });

    function chartData(start, end)
    {
        console.log(start);
        console.log(end);
        $('#chart').html();
        xhr_chart = $.ajax({
            type : 'get',
            url : "usage-data.php",
            data : 'start=' + start + '&end=' + end,
            cache : false,
            dataType : "json",
        beforeSend: function(xhr){
                if (xhr_chart != null)
                {
                    xhr_chart.abort();
                }
            }
        }).done(function(data) {
            console.log(data['days']);
            console.log(data['appliances']);

            Highcharts.chart('chart', {
                chart: {
                    type: 'spline',
                },
                title: {
                    text: ''
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: data['days']
                },
                yAxis: {
                    title: {
                        text: 'Wattage'
                    }
                },
                tooltip: {
                    crosshairs: true,
                    shared: true
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true
                        },
                        label: {
                            connectorAllowed: false
                        },
                        pointStart: 1
                    }
                },
                series: data['appliances']
            });

        }).fail(function(jqXHR, textStatus){
            //console.log('Request failed: ' + textStatus);
        });
    }
</script>
<?php include('layouts/footer-admin.php'); ?>