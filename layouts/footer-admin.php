
        </div>
        <footer class="footer">
            <div class="row">
                <div class="column-10">
                    <p class="text-center">
                        &copy; ECE, Inc <?php echo date('Y'); ?>
                    </p>
                </div>
            </div>
        </footer>
        <script type="text/javascript">
        function toggleBurger()
        {
            var burger = $('.burger');
            var menu = $('.navbar-menu');
            burger.toggleClass('is-active');
            menu.toggleClass('is-active');
        }
        </script>
    </body>
</html>