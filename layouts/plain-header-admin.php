<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <title>ECE - Electricity Consumption Estimator</title>
        <!-- Styles -->
        <link href="css/app3.css" rel="stylesheet">
        <!-- Scripts -->
        <script src="js/app.js"></script>
    </head>
    <body>