<?php 
if(isset($_GET['success'])) {
	echo '<div class="alert alert-success">' . $_GET['success'] . '</div>';
	#unset($_SESSION['success']);
} elseif(isset($_GET['warning'])) {
	echo '<div class="alert alert-danger">' . $_GET['warning'] . '</div>';
	#unset($_SESSION['warning']);
} elseif(isset($_GET['errors'])) {
?>
    <div class="alert alert-danger">
        	<?php
        	foreach (json_decode($_GET['errors']) as $error)
        	{
        		echo '<p>' . $error . '</p>';
        	}
			unset($_SESSION['errors']);
        	?>
    </div>
<?php 
}
?>