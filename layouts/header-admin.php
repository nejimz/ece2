<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <title>ECE - Electricity Consumption Estimator</title>
        <!-- Styles -->
        <link href="css/app2.css" rel="stylesheet">
        <link href="css/jquery.css" rel="stylesheet">
        <!-- Scripts -->
        <script src="js/app.js"></script>
    </head>
    <body>
        <?php
        $segment = $_SERVER['PHP_SELF'];
        $is_active = ' active';

        $home = ($segment == '/home.php')? $is_active: '';
        $calculator = ($segment == '/calculator.php')? $is_active: '';
        $usage = ($segment == '/usage.php')? $is_active: '';
        $appliances = ($segment == '/appliances.php' || $segment == '/appliances-create.php' || $segment == '/appliances-upload-image.php')? $is_active: '';
        $settings = ($segment == '/settings.php')? $is_active: '';
        ?>
        <div class="navbar">
            <a href="javascript:void(0)"><strong>ECE</strong></a>
            <a class="<?php echo $home; ?>" class="active" href="home.php"><i class="fa fa-home"></i>&nbsp;Home</a>
            <a class="<?php echo $calculator; ?>" href="calculator.php"><i class="fa fa-calculator"></i>&nbsp;Calculator</a>
            <a class="<?php echo $usage; ?>" href="usage.php"><i class="fa fa-tachometer"></i>&nbsp;Usage</a>
            <a class="<?php echo $appliances; ?>" href="appliances.php"><i class="fa fa-dropbox"></i>&nbsp;Appliances</a>
            <!-- 
            <div class="dropdown">
                <button class="dropbtn <?php #echo $appliances; ?>"><i class="fa fa-file"></i>&nbsp;Reports&nbsp;<i class="fa fa-caret-down"></i></button>
                <div class="dropdown-content">
                    <a href="bill.php"><i class="fa fa-file"></i>&nbsp;Bill</a>
                </div>
            </div>
            -->
			<?php
			if ($_SESSION['user']['role'] == 0) {
			?>
            <div class="dropdown">
                <button class="dropbtn <?php echo $settings; ?>">Settings&nbsp;<i class="fa fa-caret-down"></i></button>
                <div class="dropdown-content">
                    <a href="users.php" title="Create"><i class="fa fa-users"></i>&nbsp;User</a>
                    <!-- <a href="rate.php" title="Create"><i class="fa fa-money"></i>&nbsp;Rates</a> -->
                    <a href="rate-charges.php" title="Create"><i class="fa fa-money"></i>&nbsp;Other Charges</a>
                    <a href="advisories.php" title="Create"><i class="fa fa-file-o"></i>&nbsp;Advisories</a>
                    <a href="appliance.php" title="Create"><i class="fa fa-dropbox"></i>&nbsp;Appliances</a>
                </div>
            </div>
			<?php
			}
			?>
            <div class="topnav-right">
                <div class="dropdown">
                    <button class="dropbtn <?php echo $appliances; ?>">
                        <i class="fa fa-user"></i>&nbsp;<?php echo $_SESSION['user']['name']; ?>&nbsp;<i class="fa fa-caret-down"></i>
                    </button>
                    <div class="dropdown-content">
                        <a href="password-reset.php" title="Reset Password">Reset Password</a>
                        <a href="logout.php">Logout</a>
                    </div>
                </div> 
            </div>
        </div>
        <div class="container">